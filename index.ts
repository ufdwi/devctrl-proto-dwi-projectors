import { F32Communicator } from "./ProjectionDesign/F32Communicator"
import {ICommunicatorProtoPackage} from "@devctrl/lib-communicator";
import {IN2128Communicator} from "./InFocus/IN2128Communicator";
import {ESeriesCommunicator} from "./Christie/ESeriesCommunicator";
import {NECCommunicator} from "./NEC/NECCommunicator";
import {F90Communicator} from "./Barco/F90Communicator";
import {E2Communicator} from "./Barco/E2Communicator";

//devctrl-proto-package
module.exports = {
    communicators: {
        'ProjectionDesign/F32' : F32Communicator,
        'InFocus/IN2128' : IN2128Communicator,
        'Christie/ESeries' : ESeriesCommunicator,
        'NEC/NEC' : NECCommunicator,
        'Barco/F90' : F90Communicator,
        'Barco/E2' : E2Communicator,
    }
} as ICommunicatorProtoPackage;