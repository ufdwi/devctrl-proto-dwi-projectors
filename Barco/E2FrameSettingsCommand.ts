import {
    ITCPCommandConfig,
    JSONRPCCommand
} from "@devctrl/lib-communicator";
import {
    Control,
    ControlData
} from "@devctrl/common";


export class E2FrameSettingsCommand extends JSONRPCCommand {
    extractors = {};


    constructor(config: ITCPCommandConfig) {
        super(config);
        this.name = "Frame Settings";
    }

    static construct(endpointId: string) {
        return new E2FrameSettingsCommand(
            {
                cmdStr: "getFrameSettings",
                cmdQueryStr: "getFrameSettings",
                endpoint_id: endpointId,
                control_type: Control.CONTROL_TYPE_STRING,
                usertype: Control.USERTYPE_READONLY,
                templateConfig: {},
                poll: 1
            }
        );
    }

    public getControlTemplates() : Control[] {
        let templates : Control[] = [];
        this.ctidList = [];

        templates.push(this.registerStringControl("frameId", (response) => {
            return response.System.FrameCollection.Frame.id;
        }));
        templates.push(this.registerStringControl("Version", (response) => {
            return response.System.FrameCollection.Frame.Version;
        }));
        templates.push(this.registerStringControl("OS Version", (response) => {
            return response.System.FrameCollection.Frame.OSVersion;
        }));




        return templates;
    }


    public extractQueryResponseValue(resp, ctid: string) {
        if (this.extractors[ctid]) {
            return this.extractors[ctid](resp.result.response);
        }

        throw new Error("no extractor function defined for " + ctid);
    }


    public queryObject() {
        return {
            jsonrpc: "2.0",
            id: `q-${this.cmdStr}`,
            method: this.cmdQueryStr,
            params: {}
        };
    }

    private registerStringControl(name: string, extractor: (resp: any) => any) : Control {
        let ctid = this.endpoint_id + "-" + this.cmdStr + "-" + name;

        this.extractors[ctid] = extractor;
        let controlData : ControlData = {
            _id: ctid,
            ctid: ctid,
            endpoint_id: this.endpoint_id,
            usertype: Control.USERTYPE_READONLY,
            name: name,
            control_type: Control.CONTROL_TYPE_STRING,
            poll: this.poll,
            ephemeral: this.ephemeral,
            config: {},
            value: 0
        };

        this.ctidList.push(ctid);

        return new Control(ctid, controlData);
    }
}