import {BarcoRPCCommand, F90PowerCommand} from "./BarcoRPCCommand";
import {BarcoRPCCommunicator} from "./BarcoRPCCommunicator";
import {EndpointCommunicator} from "@devctrl/lib-communicator";
import {F90BooleanCommand} from "./F90BooleanCommand";


export class F90Communicator extends BarcoRPCCommunicator {



    buildCommandList() {
        this.commands["system.state"] = F90PowerCommand.construct(this.endpoint_id);
        this.commands["shutter"] = new F90BooleanCommand(this.endpoint_id,
            "Shutter",
            "optics.shutter.target",
            "Closed",
            "Open");
        this.registerSelectCommand("Source", "image.window.main.source",
            {
                "DVI 1" : "DVI 1",
                "DVI 2" : "DVI 2",
                "DisplayPort 1" : "DisplayPort 1",
                "DisplayPort 2" : "DisplayPort 2",
                "Dual DVI" : "Dual DVI",
                "Dual DisplayPort" : "Dual DisplayPort",
                "HDMI" : "HDMI",
                "HDBaseT" : "HDBaseT"
            });
        this.registerRangeCommand("Brightness", "image.brightness", -1, 1, 0.02);
        this.registerRangeCommand("Contrast", "image.contrast", 0, 2, 0.02);
        this.registerRangeCommand("Saturation", "image.saturation", 0, 2, 0.02);
        //P7 RealColor Mode controls: not implemented
        this.registerSelectCommand("Output Resolution", "image.resolution.resolution",
            {
               "WQXGA" : "WQXGA",
               "4K-UHD": "4K-UHD"
            });
        this.registerMultibuttonCommand("Lens Shift Up",
            "optics.lensshift.vertical.stepreverse",
            {
                buttonOrientation: "up",
                buttonValues: {
                    1: 1,
                    2: 5,
                    3: 10
                }
            });
        this.registerMultibuttonCommand("Lens Shift Down",
            "optics.lensshift.vertical.stepforward",
            {
                buttonOrientation: "down",
                buttonValues: {
                    1: 1,
                    2: 5,
                    3: 10
                }
            });
        this.registerMultibuttonCommand("Lens Shift Left",
            "optics.lensshift.horizontal.stepreverse",
            {
                buttonOrientation: "left",
                buttonValues: {
                    1: 1,
                    2: 5,
                    3: 10
                }
            });
        this.registerMultibuttonCommand("Lens Shift Right",
            "optics.lensshift.horizontal.stepforward",
            {
                buttonOrientation: "right",
                buttonValues: {
                    1: 1,
                    2: 5,
                    3: 10
                }
            });
        this.registerMultibuttonCommand("Zoom In",
            "optics.zoom.stepforward",
            {
                buttonOrientation: "up",
                buttonValues: {
                    1: 1,
                    2: 5,
                    3: 10
                }
            });
        this.registerMultibuttonCommand("Zoom Out",
            "optics.zoom.stepreverse",
            {
                buttonOrientation: "down",
                buttonValues: {
                    1: 1,
                    2: 5,
                    3: 10
                }
            });
        this.registerMultibuttonCommand("Focus In",
            "optics.focus.stepforward",
            {
                buttonOrientation: "up",
                buttonValues: {
                    1: 1,
                    2: 5,
                    3: 10
                }
            });
        this.registerMultibuttonCommand("Focus Out",
            "optics.focus.stepreverse",
            {
                buttonOrientation: "down",
                buttonValues: {
                    1: 1,
                    2: 5,
                    3: 10
                }
            });

    }


    /**
     * Send a single request for all of the polled values
     */
    poll() {
        if (! this.epStatus.polling) {
            return;
        }

        let request = {
            jsonrpc : "2.0",
            id: "poll",
            method: "property.get",
            params : {
                property: []
            }
        };

        for (let id in this.controls) {
            let control = this.controls[id];

            if (control.poll) {
                let cmd = this.commandsByTemplate[control.ctid];

                if (cmd) {
                    // Don't request the same value multiple times
                    if (request.params.property.indexOf(cmd.cmdStr) == -1) {
                        request.params.property.push(cmd.cmdStr);
                    }
                }
                else {
                    this.log("command not found for poll control " + control.ctid);
                }
            }
        }

        this.responseCallbacks["poll"] = (resp) => {
            if (resp.result == undefined) {
                this.log("invalid poll response, result is not defined", EndpointCommunicator.LOG_POLLING);
                return;
            }
            if (typeof resp.result !== 'object') {
                this.log("invalid poll response, result is not an object", EndpointCommunicator.LOG_POLLING);
                return;
            }

            for (let param in resp.result) {
                if (this.commands[param]) {
                    for (let ctid of this.commands[param].ctidList) {
                        let control = this.controlsByCtid[ctid];
                        let val = this.commands[param].extractQueryResponseValue(resp, ctid);
                        this.setControlValue(control, val);
                    }
                }
                else {
                    this.log(`unexpected poll result for param ${param}`, EndpointCommunicator.LOG_POLLING);
                }
            }
        };

        this.writeToSocket(JSON.stringify(request));
    }
}