import {BarcoRPCCommunicator} from "./BarcoRPCCommunicator";
import {E2FrameSettingsCommand} from "./E2FrameSettingsCommand";
import {EndpointCommunicator, JSONRPCCommand} from "@devctrl/lib-communicator";
import * as http from "http";
import {E2PresetsCommand} from "./E2PresetsCommand";


export class E2Communicator extends BarcoRPCCommunicator {
    connectionProtocol = "http-post";

    buildCommandList() {
        this.commands["getFrameSettings"] = E2FrameSettingsCommand.construct(this.endpoint_id);
        this.commands["listPresets"] = new E2PresetsCommand(this.endpoint_id);
    }

    closeConnection() {
        // Not really much to do here, no persistent connection is maintained
        this.updateStatus({
            polling: false,
            responsive: false
        });

    };


    executeCommandQuery(cmd: JSONRPCCommand) {
        if (cmd.writeonly) {
            this.log(`not querying writeonly command ${cmd.name}`, EndpointCommunicator.LOG_POLLING);
        }

        let requestPath = "http://" + this.config.endpoint.address + ":" + this.config.endpoint.port;
        this.log("sending request:" + requestPath, EndpointCommunicator.LOG_RAW_DATA);

        let queryObject = cmd.queryObject();
        let postData = JSON.stringify(queryObject);

        this.responseCallbacks[queryObject.id] = (resp) => {

            for (let ctid of cmd.ctidList) {
                try {
                    let val = cmd.extractQueryResponseValue(resp, ctid);
                    let control = this.controlsByCtid[ctid];
                    //debug("control id is " + control._id);
                    this.setControlValue(control, val);
                }
                catch(e) {
                    this.log(`Error extracting value for ${ctid}: ${e.message}`);
                }
            }
        };


        let req = http.request(
            {
                hostname: this.config.endpoint.address,
                port: this.config.endpoint.port,
                method: "POST",
                headers: {
                    "Content-Type" : "application/json",
                    "Content-Length" : Buffer.byteLength(postData)
                }
            },
            (res) => {
                if (res.statusCode !== 200) {
                    this.log("invalid status code response: " + res.statusCode);
                }
                else {
                    //debug(`cmd ${cmd.name} successfully queried`);
                    res.setEncoding('utf8');
                    let body ='';
                    res.on('data', (chunk) => { body += chunk});
                    res.on('end', () => {
                        this.onData(body);
                    });
                }
            })
            .on('error', (e) => {
                this.log(`Error on query: ${e.message}`);
                this.closeConnection();
            });

        req.write(postData);
        req.end();
    }


    openConnection() {
        this.updateStatus({connected: true});
    }
}