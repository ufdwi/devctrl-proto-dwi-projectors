import {
    EndpointCommunicator,
    JSONRPCCommunicator
} from "@devctrl/lib-communicator";
import {
    ControlUpdateData
} from "@devctrl/common";
import {BarcoRPCCommand} from "./BarcoRPCCommand";
import * as http from "http";


export class BarcoRPCCommunicator extends JSONRPCCommunicator {
    connectionProtocol = "tcp";


    handleControlUpdateRequest(request: ControlUpdateData) {
        let control = this.controls[request.control_id];

        if (! this.epStatus.ok) {
            // Update can't be processed, remind
            this.log("update cannot be processed, communicator not connected", EndpointCommunicator.LOG_UPDATES);
            this.config.statusUpdateCallback();
            return;
        }

        let command = this.commandsByTemplate[control.ctid];

        if (command) {
            let updateObj = command.updateObject(control, request);

            this.responseCallbacks[updateObj.id] = (resp) => {
                let val = command.extractUpdateResponseValue(request, resp);
                this.setControlValue(control, val);
            };

            let updateStr = JSON.stringify(updateObj);
            if (this.connectionProtocol == "tcp") {
                this.writeToSocket(updateStr);
            }
            else if (this.connectionProtocol == "http-post") {
                let req = http.request(
                    {
                        hostname: this.config.endpoint.address,
                        port: this.config.endpoint.port,
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                            "Content-Length": Buffer.byteLength(updateStr)
                        }
                    },
                    (res) => {
                        if (res.statusCode !== 200) {
                            this.log("invalid status code response: " + res.statusCode);
                        }
                        else {
                            //debug(`cmd ${cmd.name} successfully queried`);
                            res.setEncoding('utf8');
                            let body ='';
                            res.on('data', (chunk) => { body += chunk});
                            res.on('end', () => {
                                this.onData(body);
                            });
                        }
                    }
                );

                req.on('error', (e) => {
                    this.log(`Error on query: ${e.message}`);
                    this.closeConnection();
                });

                this.log(`sending call: ${updateStr}`, EndpointCommunicator.LOG_RAW_DATA);
                req.write(updateStr);
                req.end();
            }
        }
    }




    registerRangeCommand(name: string, propertyName: string, min: number, max: number, step: number = 1) {
        this.commands[propertyName] = BarcoRPCCommand.rangeCommand(
            this.endpoint_id,
            name,
            propertyName,
            min,
            max,
            step
        );
    }


    registerSelectCommand(name: string, propertyName: string, options) {
        this.commands[propertyName] = BarcoRPCCommand.selectCommand(
            this.endpoint_id,
            name,
            propertyName,
            options
        );
    }

    registerMultibuttonCommand(name: string, propertyName: string, options) {
        this.commands[propertyName] = BarcoRPCCommand.multiButtonCommand(
            this.endpoint_id,
            name,
            propertyName,
            options
        );
    }
}