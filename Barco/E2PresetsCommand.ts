import {
    ITCPCommandConfig,
    JSONRPCCommand
} from "@devctrl/lib-communicator";
import {
    Control,
    ControlData,
    ControlUpdateData
} from "@devctrl/common";


export class E2PresetsCommand extends JSONRPCCommand {
    extractors = {};
    listCtid = 'listPresets';
    optionsCtid = 'presetOptions';
    activateProgramCtid = 'activateProgramPreset';
    activatePreviewCtid = 'activatePreviewCtid';


    constructor(endpointId: string) {
        super({
            cmdStr: "listPresets",
            cmdQueryStr: "listPresets",
            endpoint_id: endpointId,
            control_type: Control.CONTROL_TYPE_STRING,
            usertype: Control.USERTYPE_SELECT,
            templateConfig: {},
            poll: 1
        });
        this.name = "List Presets";
        this.listCtid = endpointId + "-" + this.listCtid;
        this.optionsCtid = endpointId + "-" + this.optionsCtid;
        this.activateProgramCtid = endpointId + "-" + this.activateProgramCtid;
        this.activatePreviewCtid = endpointId + "-" + this.activatePreviewCtid;
    }


    public extractQueryResponseValue(resp, ctid: string) {
        let response = resp.result.response;

        if (ctid == this.listCtid) {
            return response;
        }
        else if (ctid == this.optionsCtid) {
            // Reformat the presets list to be
            let options = [];
            for (let preset of response) {
                options.push({
                    name: preset.Name,
                    value: preset.presetSno
                });
            }

            return options;
        }
        else if (ctid == this.activatePreviewCtid || ctid == this.activateProgramCtid) {
            //TODO: this is not ideal, but we don't know how to query the value of this control
            throw new Error("no actual problem here");
        }

        return '';
    }

    extractUpdateResponseValue(controlUpdate : ControlUpdateData, resp : any) {
        if (resp.result.success == 0) {
            return controlUpdate.value;
        }
        else {
            throw new Error("unexpected update result")
        }

    }

    public getControlTemplates() : Control[] {
        let ctid = this.listCtid;
        this.ctidList = [ctid];
        let templates = [];
        templates.push(new Control(ctid,
            {
                _id: ctid,
                ctid: ctid,
                endpoint_id: this.endpoint_id,
                usertype: Control.USERTYPE_TREE_READONLY,
                name: "Preset Data",
                control_type: Control.CONTROL_TYPE_OBJECT,
                poll: this.poll,
                ephemeral: this.ephemeral,
                config: {},
                value: 0
            }));

        this.ctidList.push(this.optionsCtid);
        templates.push(new Control(this.optionsCtid,
            {
                _id: this.optionsCtid,
                ctid: this.optionsCtid,
                endpoint_id: this.endpoint_id,
                usertype: Control.USERTYPE_TREE_READONLY,
                name: "Preset Options",
                control_type: Control.CONTROL_TYPE_OBJECT,
                poll: this.poll,
                ephemeral: this.ephemeral,
                config: {},
                value: {}
            }));

        this.ctidList.push(this.activateProgramCtid);
        templates.push(new Control(this.activateProgramCtid,
            {
                _id: this.activateProgramCtid,
                ctid: this.activateProgramCtid,
                endpoint_id: this.endpoint_id,
                usertype: Control.USERTYPE_SELECT,
                name: "Activate Preset (Program)",
                control_type: Control.CONTROL_TYPE_FLOAT,
                poll: this.poll,
                ephemeral: this.ephemeral,
                config: {
                    optionsCtid: this.optionsCtid
                },
                value: {}
            }));

        this.ctidList.push(this.activatePreviewCtid);
        templates.push(new Control(this.activatePreviewCtid,
            {
                _id: this.activatePreviewCtid,
                ctid: this.activatePreviewCtid,
                endpoint_id: this.endpoint_id,
                usertype: Control.USERTYPE_SELECT,
                name: "Activate Preset (Preview)",
                control_type: Control.CONTROL_TYPE_FLOAT,
                poll: this.poll,
                ephemeral: this.ephemeral,
                config: {
                    optionsCtid: this.optionsCtid
                },
                value: {}
            }));

        return templates;

    }


    public queryObject() {
        return {
            jsonrpc: "2.0",
            id: `q-${this.cmdStr}`,
            method: this.cmdQueryStr,
            params: {}
        };
    }


    updateObject(control: Control, update: ControlUpdateData) : any {
        let type = control.ctid == this.activatePreviewCtid ? 0 : 1;

        return {
            jsonrpc: "2.0",
            id: update._id,
            method: "activatePreset",
            params: {
                presetSno: update.value,
                type: type  //"type”—0 to recall in preview (default), 1 to recall in program.
                //This is not a mandatory parameter but should be given when the user wants to recall a Preset in program
            }
        }
    }

}