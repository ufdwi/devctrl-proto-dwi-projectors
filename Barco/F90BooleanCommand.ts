import {
    Control
} from "@devctrl/common";
import {BarcoRPCCommand} from "./BarcoRPCCommand";


export class F90BooleanCommand extends BarcoRPCCommand {
    trueVal: string;
    falseVal: string;

    constructor(endpointId: string,
                name: string,
                propertyName: string,
                trueVal: string,
                falseVal: string) {
        super({
            cmdStr: propertyName,
            cmdQueryStr: "property.get",
            endpoint_id: endpointId,
            control_type: Control.CONTROL_TYPE_BOOLEAN,
            usertype: Control.USERTYPE_SWITCH,
            templateConfig: {},
            cmdQueryParams: {
                "property" : propertyName
            },
            cmdUpdateMethod: "property.set",
            cmdUpdateParams: {
                property: propertyName,
                value : ""
            },
            setUpdateValueFunction: (obj, val)=> {
                obj.params.value = val ? trueVal : falseVal;
                return obj;
            },
            name: name,
            poll: 1
        });

        this.trueVal = trueVal;
        this.falseVal = falseVal;
    }


    extractQueryResponseValue(resp: any, ctid: string) {
        if (resp.result == undefined) {
            throw new Error("undefined query result");
        }

        if (resp.result[this.cmdStr] !== undefined) {
            return resp.result[this.cmdStr] == this.trueVal;
        }


        return resp.result == this.trueVal;
    }
}