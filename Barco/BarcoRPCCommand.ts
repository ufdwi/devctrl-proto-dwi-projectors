import {
    ArrowButtonSetControlConfig,
    Control,
    ControlUpdateData
} from "@devctrl/common";
import {
    TCPCommand,
    ITCPCommandConfig,
    JSONRPCCommand
} from "@devctrl/lib-communicator";



export interface IBarcoRPCCommandConfig extends ITCPCommandConfig {
    cmdQueryParams? : any;
    cmdUpdateMethod: string;
    cmdUpdateParams : any;
    setUpdateValueFunction : (obj: any, val: any) => any;
    name: string;
}


export class BarcoRPCCommand extends JSONRPCCommand {
    cmdQueryParams : any = false;
    cmdUpdateMethod : string;
    cmdUpdateParams : any;
    setUpdateValueFunction : (obj: any, val: any) => any;

    constructor(config : IBarcoRPCCommandConfig) {
        super(config);

        this.name = config.name;

        if (config.cmdQueryParams) {
            this.cmdQueryParams = config.cmdQueryParams;
        }
        else {
            this.writeonly = true;
        }

        this.cmdUpdateMethod = config.cmdUpdateMethod;
        this.cmdUpdateParams = config.cmdUpdateParams;
        this.setUpdateValueFunction = config.setUpdateValueFunction;
    }

    static basicCommand(endpointId: string,
                        name: string,
                        propertyName: string,
                        controlType: string,
                        usertype: string,
                        templateConfig) {
        return new BarcoRPCCommand({
            cmdStr: propertyName,
            cmdQueryStr: "property.get",
            endpoint_id: endpointId,
            control_type: controlType,
            usertype: usertype,
            templateConfig: templateConfig,
            cmdQueryParams: {
                "property" : propertyName
            },
            cmdUpdateMethod: "property.set",
            cmdUpdateParams: {
                property: propertyName,
                value : ""
            },
            setUpdateValueFunction: (obj, val)=> {
                obj.params.value = val;
                return obj;
            },
            name: name,
            poll: 1
        });
    }

    extractQueryResponseValue(resp: any, ctid: string) {
        if (resp.result == undefined) {
            throw new Error("undefined query result");
        }

        if (resp.result[this.cmdStr] !== undefined) {
            return resp.result[this.cmdStr];
        }

        return resp.result;
    }

    extractUpdateResponseValue(controlUpdate : ControlUpdateData, resp : any) {
        if (resp.result === true || resp.result === null) {
            return controlUpdate.value;
        }
        else {
            throw new Error("unexpected update result")
        }

    }


    queryObject() {
        return {
            jsonrpc: "2.0",
            id: `q-${this.cmdStr}`,
            method: this.cmdQueryStr,
            params: this.cmdQueryParams
        };
    }

    static rangeCommand(endpointId: string, name: string, propertyName : string, min: number, max: number, step: number = 1) {
            return BarcoRPCCommand.basicCommand(
                endpointId,
                name,
                propertyName,
                Control.CONTROL_TYPE_RANGE,
                Control.USERTYPE_SLIDER,
                {
                    min: min,
                    max: max,
                    step: step
                }
            );
    }

    static multiButtonCommand(endpointId: string, name: string, propertyName: string, options: ArrowButtonSetControlConfig) {
        return new BarcoRPCCommand({
            cmdStr: propertyName,
            cmdQueryStr: "property.get",
            endpoint_id: endpointId,
            control_type: Control.CONTROL_TYPE_INT,
            usertype: Control.USERTYPE_ARROW_BUTTON_SET,
            templateConfig: options,
            cmdQueryParams: {
                "property" : propertyName
            },
            cmdUpdateMethod: "property.set",
            cmdUpdateParams: {
                property: propertyName,
                value : ""
            },
            setUpdateValueFunction: (obj, val)=> {
                obj.params.value = val;
                return obj;
            },
            name: name,
            poll: 0,
            writeonly: true
        });
    }


    static selectCommand(endpointId: string, name: string, propertyName: string, options) {
        return BarcoRPCCommand.basicCommand(
            endpointId,
            name,
            propertyName,
            Control.CONTROL_TYPE_STRING,
            Control.USERTYPE_SELECT,
            { options: options}
        )
    }

    updateObject(control : Control, update: ControlUpdateData) {
        let upObj = {
            jsonrpc: "2.0",
            id: update._id,
            method: this.cmdUpdateMethod,
            params: this.cmdUpdateParams
        };

        upObj = this.setUpdateValueFunction(upObj, update.value);
        return upObj;
    }
}

/**
 *  The power command has it's own class on account of "power on" and "power off" being 2 distinct commands
 */

export class F90PowerCommand extends JSONRPCCommand {
    constructor(config: ITCPCommandConfig) {
        super(config);
        this.name = "Power";
    }

    static construct(endpointId: string) {
        return new F90PowerCommand(
            {
                cmdStr: "system.state",
                cmdQueryStr: "property.get",
                endpoint_id: endpointId,
                control_type: Control.CONTROL_TYPE_BOOLEAN,
                usertype: Control.USERTYPE_SWITCH,
                templateConfig: {},
                poll: 1
            }
        );
    }


    extractQueryResponseValue(resp: any, ctid: string) {
        if (resp.result == undefined) {
            throw new Error("undefined query result");
        }

        if (resp.result[this.cmdStr] !== undefined) {
            return resp.result[this.cmdStr] == "on";
        }

        return resp.result == "on";
    }

    extractUpdateResponseValue(controlUpdate : ControlUpdateData, resp : any) {
        if (resp.result === true) {
            return controlUpdate.value;
        }
        else {
            throw new Error("unexpected update result")
        }

    }

    queryObject() {
        return {
            jsonrpc: "2.0",
            id: `query-${this.cmdStr}`,
            method: this.cmdQueryStr,
            params: { property: this.cmdStr }
        };
    }


    updateObject(control : Control, update: ControlUpdateData) {
        let method = update.value ? "system.poweron" : "system.poweroff";

        return {
            jsonrpc: "2.0",
            id: update._id,
            method: method,
            params: {}
        };
    }
}


